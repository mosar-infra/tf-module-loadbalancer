output "lb_target_group" {
  value = aws_lb_target_group.target_group
}

output "lb_listener" {
  value = aws_lb_listener.listener
}

output "lb" {
  value = aws_lb.lb
}
