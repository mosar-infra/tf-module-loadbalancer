# Tf Module Loadbalancer

Module to create one loadbalancer. To create multiple use for_each in the calling workspace.
Expects variables for the lb itself as well as `environment` and `managed_by` for tagging.
