# loadbalancing/variables.tf

variable "lb_name" {}
variable "public_subnet_ids" {}
variable "public_security_group_ids" {}
variable "environment" {}
variable "vpc_id" {}
variable "lb_tg_name" {}
variable "lb_tg_port" {}
variable "lb_tg_protocol" {}
variable "lb_tg_target_type" {}
variable "lb_tg_target_id" {
  default = ""
}
variable "lb_tg_healthy_threshold" {}
variable "lb_tg_unhealthy_threshold" {}
variable "lb_tg_path" {}
variable "lb_tg_timeout" {}
variable "lb_tg_interval" {}
variable "add_attachment" {}
variable "listener_port" {}
variable "listener_protocol" {}
variable "ssl_policy" {}
variable "certificate_arn" {}
variable "managed_by" {}
