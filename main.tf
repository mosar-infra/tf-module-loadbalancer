# loadbalancing/main.tf

resource "aws_lb" "lb" {
  name               = var.lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = var.public_security_group_ids
  subnets            = var.public_subnet_ids
  idle_timeout       = 100
  tags = {
    Environment = var.environment
    ManagedBy = var.managed_by
  }
}


resource "aws_lb_target_group" "target_group" {
  name     = "${var.lb_tg_name}-${substr(uuid(), 0, 3)}"
  port     = var.lb_tg_port
  protocol = var.lb_tg_protocol
  target_type = var.lb_tg_target_type
  vpc_id   = var.vpc_id
  health_check {
    healthy_threshold   = var.lb_tg_healthy_threshold
    unhealthy_threshold = var.lb_tg_unhealthy_threshold
    path                = var.lb_tg_path
    timeout             = var.lb_tg_timeout
    interval            = var.lb_tg_interval
  }
  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }
  tags = {
    Environment = var.environment
    ManagedBy = var.managed_by
  }
}

resource "aws_lb_target_group_attachment" "attachment" {
  count            = var.add_attachment == true ? 1 : 0
  target_group_arn = aws_lb_target_group.target_group.arn
  target_id        = var.lb_tg_target_id
  port             = var.lb_tg_port
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = var.listener_port
  protocol          = var.listener_protocol
  ssl_policy        = var.ssl_policy
  certificate_arn   = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.target_group.arn
  }
  tags = {
    Environment = var.environment
    ManagedBy = var.managed_by
  }
}


